package de.cb.home.telegram.server.resources.admin;

import de.cb.home.telegram.bot.common.PropertiesHelper;
import de.cb.home.telegram.bot.common.ScoreBotConstants;
import de.cb.home.telegram.bot.model.telegramapi.model.Message;
import de.cb.home.telegram.bot.model.telegramapi.model.Update;
import de.cb.home.telegram.bot.model.telegramapi.model.Updates;
import de.cb.home.telegram.server.MatchBotConfig;
import de.cb.home.telegram.server.settings.Chat;
import de.cb.home.telegram.server.settings.ChatSettingsHelper;
import de.cb.home.telegram.server.views.SettingsView;
import okhttp3.OkHttpClient;
import okhttp3.Request;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Path(ScoreBotConstants.SETTINGS_PATH)
public class SettingsResource {

   private ObjectMapper objMapper;
   private MatchBotConfig configuration;
   private PropertiesHelper settingsHelper;
   private ChatSettingsHelper chatsHelper;

   public SettingsResource(MatchBotConfig configuration, ChatSettingsHelper chatsHelper,
         PropertiesHelper settingsHelper) {
      this.configuration = configuration;
      this.chatsHelper = chatsHelper;
      this.settingsHelper = settingsHelper;
      objMapper = new ObjectMapper();
   }

   @GET
   @Path("choose")
   public Response settings(@QueryParam("chat_id") Integer chatId, @QueryParam("debug") Boolean debug) {
      try {
         if (chatId != null) {
            settingsHelper.setProperty(PropertiesHelper.SETTINGS_CHAT_ID, chatId.toString());
         }

         if (debug != null) {
            settingsHelper.setProperty(PropertiesHelper.SETTINGS_DEBUG, debug.toString());
         }
      } catch (IOException e) {
         e.printStackTrace();
         return Response.status(Status.INTERNAL_SERVER_ERROR)
               .build();
      }
      return Response.ok("value updated")
            .build();
   }

   @GET
   public SettingsView getTelegramUpdates() throws Exception {
      Map<String, Chat> chatsFromUpdates;
      try {
         chatsFromUpdates = getChatsFromUpdates();
      } catch (Exception e) {
         e.printStackTrace();
         chatsFromUpdates = null;
      }
      if (chatsFromUpdates != null) {
         try {
            chatsHelper.putAll(chatsFromUpdates);
         } catch (IOException e) {
            e.printStackTrace();
            return null;
         }
      }
      String debugMode = settingsHelper.getProperty(PropertiesHelper.SETTINGS_DEBUG);
      return new SettingsView(ScoreBotConstants.SETTINGS_TEMPLATE, chatsHelper.getChats(), debugMode);
   }

   private Map<String, Chat> getChatsFromUpdates() throws JsonParseException, JsonMappingException, IOException {
      OkHttpClient client = new OkHttpClient();
      String telegramUpdatesUrl = ScoreBotConstants.TELEGRAM_UPDATES_URL;
      Request request = new Request.Builder().url(telegramUpdatesUrl)
            .build();
      okhttp3.Response responses = null;

      responses = client.newCall(request)
            .execute();
      String jsonData = responses.body()
            .string();
      Updates updates = objMapper.readValue(jsonData, Updates.class);
      Map<String, Chat> groupChats = new HashMap<>();
      Map<String, Chat> singleChats = new HashMap<>();
      for (Update update : updates.getResult()) {
         Message message = update.getMessage();
//         if (message.getChat()
         //               .getTitle() != null) {
         //            groupChats.put(message.getChat()
         //                  .getId(), new Chat(message.getChat()
         //                  .getTitle()));
         //         }
      }

      for (Update update : updates.getResult()) {
         Message message = update.getMessage();
//         if (message.getChat()
//               .getTitle() == null) {
//            singleChats.put(message.getChat()
//                        .getId(),
//                  new Chat(message.getChat()
//                        .getFirstName() + " " + message.getChat()
//                        .getLastName()));
//         }
      }

      groupChats.putAll(singleChats);
      return groupChats;
   }

   @GET
   @Path("remove")
   public Response removeChat(@QueryParam("chat_id") String chatId)
         throws JsonParseException, JsonMappingException, IOException {
      chatsHelper.removeChat(chatId);
      return Response.ok()
            .build();
   }

   @GET
   @Path("active")
   public Response setChatActive(@QueryParam("chat_id") String chatId, @QueryParam("state") Boolean state)
         throws JsonParseException, JsonMappingException, IOException {
      chatsHelper.setChatActive(chatId, state);
      return Response.ok()
            .build();
   }
}
