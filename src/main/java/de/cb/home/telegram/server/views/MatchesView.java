package de.cb.home.telegram.server.views;

import de.cb.home.telegram.parser.hltv.MatchesOverview;
import io.dropwizard.views.View;

public class MatchesView extends View {
   private MatchesOverview matchesOverview;

   public MatchesView(String templateName, MatchesOverview matchesOverview) {
      super(templateName);
      this.matchesOverview = matchesOverview;
   }

   public MatchesOverview getMatchesOverview() {
      return matchesOverview;
   }
}
