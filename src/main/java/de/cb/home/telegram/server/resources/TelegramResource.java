package de.cb.home.telegram.server.resources;

import de.cb.home.telegram.bot.common.ScoreBotConstants;
import de.cb.home.telegram.bot.model.telegramapi.model.Update;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path(ScoreBotConstants.TELEGRAM_PATH)
public class TelegramResource {

   @POST
   @Consumes(MediaType.APPLICATION_JSON)
   @Produces(MediaType.APPLICATION_JSON)
   public Response getLiveMatches(Update update) {
      System.out.println("called live through telegram POST");
      //		String chatId = update.getMessage().getChat().getId();
      //		String text = update.getMessage().get;

      //      Message message = update.getMessage();
      //      boolean isTextMessage = message.getText() != null;
      return Response.ok()
            .build();
   }

}
