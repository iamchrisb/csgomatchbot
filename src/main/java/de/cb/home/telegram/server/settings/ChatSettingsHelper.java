package de.cb.home.telegram.server.settings;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ChatSettingsHelper {

	Map<String, Chat> chats = new HashMap<>();
	private String jsonPath;
	private ObjectMapper mapper;
	TypeReference<HashMap<String, Chat>> typeRef = new TypeReference<HashMap<String, Chat>>() {
	};
	private File fileRef;

	public ChatSettingsHelper(String jsonPath) throws IOException {
		this.jsonPath = jsonPath;
		mapper = new ObjectMapper();
		fileRef = new File(jsonPath);
		if (!fileRef.exists()) {
			boolean createNewFile = fileRef.createNewFile();
			System.out.println(createNewFile);
		}
	}

	public Map<String, Chat> load() throws JsonParseException, JsonMappingException, IOException {
		return mapper.readValue(fileRef, typeRef);
	}

	public void save(Map<String, Chat> chats) throws JsonGenerationException, JsonMappingException, IOException {
		mapper.writeValue(fileRef, chats);
	}

	public Chat getChat(String id) throws JsonParseException, JsonMappingException, IOException {
		this.chats = load();
		return chats.get(id);
	}

	public void setChat(String id, Chat chat) throws JsonParseException, JsonMappingException, IOException {
		try {
			this.chats = load();
		} catch (JsonMappingException e) {
			// TODO the file could be just empty at this point
			e.printStackTrace();
		}
		chats.put(id, chat);
		save(chats);
	}

	public void setChatActive(String id, boolean state) throws JsonParseException, JsonMappingException, IOException {
		Chat chat = chats.get(id);
		chat.setActive(state);
		setChat(id, chat);
	}

	public void putAll(Map<String, Chat> chats) throws JsonParseException, JsonMappingException, IOException {
		this.chats = load();
		this.chats.putAll(chats);
		save(chats);
	}

	public Chat removeChat(String id) throws JsonParseException, JsonMappingException, IOException {
		this.chats = load();
		Chat removedChat = this.chats.remove(id);
		save(chats);
		return removedChat;
	}

	public Map<String, Chat> getChats() throws JsonParseException, JsonMappingException, IOException {
		try {
			this.chats = load();
		} catch (JsonMappingException e) {
			// TODO fakk
		}
		return chats;
	}

}
