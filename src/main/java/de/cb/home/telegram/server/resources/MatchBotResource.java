package de.cb.home.telegram.server.resources;

import de.cb.home.telegram.bot.ScoreBotEngine;
import de.cb.home.telegram.bot.common.PropertiesHelper;
import de.cb.home.telegram.bot.common.ScoreBotConstants;
import de.cb.home.telegram.parser.HltvMatchesOverviewParser;
import de.cb.home.telegram.parser.hltv.MatchesOverview;
import de.cb.home.telegram.parser.model.LiveMatch;
import de.cb.home.telegram.server.MatchBotConfig;
import de.cb.home.telegram.server.settings.ChatSettingsHelper;
import de.cb.home.telegram.server.views.MatchesView;
import de.cb.home.telegram.server.views.TeamsView;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.joda.time.DateTime;

@Path("/")
public class MatchBotResource {

   private MatchBotConfig config;

   private static final int FIVE_MINUTES = 300000;
   private ScoreBotEngine engine;

   private long timeStamp = 0;
   private PropertiesHelper settingsHelper;
   private PropertiesHelper teamsHelper;

   private ChatSettingsHelper chatsHelper;

   private List<LiveMatch> liveMatches;

   public MatchBotResource(MatchBotConfig config, PropertiesHelper settingsHelper, PropertiesHelper teamsHelper,
         ChatSettingsHelper chatsHelper) {
      this.config = config;
      this.settingsHelper = settingsHelper;
      this.teamsHelper = teamsHelper;
      this.chatsHelper = chatsHelper;

      // runningBots = new HashMap<String, Pair<ScoreBotEngine,
      // List<String>>>();

      matchesParser = new HltvMatchesOverviewParser();
   }

   @POST
   @Path("/ticker/{matchId}")
   public Response addTicker(@PathParam("matchId") String matchId, @QueryParam("chatId") String chatId)
         throws URISyntaxException {

      ScoreBotFacade.getInstance()
            .startTicker(matchId, chatId);

      return Response.ok()
            .build();
   }

   @GET
   @Path("/matches")
   public MatchesView matches() {
      DateTime time = new DateTime();
      long currentTimeStamp = time.getMillis();

      if (currentTimeStamp - timeStamp > FIVE_MINUTES || timeStamp == 0) {
         timeStamp = currentTimeStamp;
         try {
            matchesOverview = matchesParser.fetchLiveMatchesFromHTML();
         } catch (Exception e) {
            e.printStackTrace();
            return new MatchesView("random.ftl", null);
         }
      }

      if (matchesOverview != null) {
         return new MatchesView(ScoreBotConstants.HLTV_MATCHES_TEMPLATE, matchesOverview);
      } else {
         return new MatchesView("random.ftl", null);
      }
   }

   @GET
   @Path("/t-live")
   public Response tLive() {
      DateTime time = new DateTime();
      long currentTimeStamp = time.getMillis();

      if (currentTimeStamp - timeStamp > FIVE_MINUTES || timeStamp == 0) {
         timeStamp = currentTimeStamp;
         //         liveMatches = parser.fetchLiveMatchesFromHTML();
      }
      if (liveMatches != null) {
         StringBuilder sb = new StringBuilder();
         for (LiveMatch liveMatch : liveMatches) {
            sb.append(liveMatch.toString() + "</br>");
         }
         return Response.ok(sb.toString())
               .build();
      }
      return Response.ok("no live matches available")
            .build();
   }

   @GET
   @Path("/tick")
   public Response tick(@QueryParam("match_id") String matchId) {
      try {
         if (engine != null) {
            engine.cancel();
         }

         engine = new ScoreBotEngine(settingsHelper, teamsHelper);
         String chatId = settingsHelper.getProperty(PropertiesHelper.SETTINGS_CHAT_ID);

         if (chatId == null) {
            return Response.status(Status.INTERNAL_SERVER_ERROR)
                  .entity("no chat_id was set")
                  .build();
         }

         engine.update(matchId);
      } catch (Exception e) {
         return Response.status(Status.INTERNAL_SERVER_ERROR)
               .entity("something went wrong")
               .build();
      }
      return Response.ok("this is matchbot")
            .build();
   }

   @GET
   @Path("maps")
   public Response maps() {
      if (engine == null) {
         return Response.status(Status.BAD_REQUEST)
               .entity("No engine has been started")
               .build();
      }
      String chatId = settingsHelper.getProperty(PropertiesHelper.SETTINGS_CHAT_ID);
      engine.tickMaps(chatId);
      return Response.ok()
            .build();
   }

   @GET
   @Path("players")
   public Response players() {
      if (engine == null) {
         return Response.status(Status.BAD_REQUEST)
               .entity("No engine has been started")
               .build();
      }
      String chatId = settingsHelper.getProperty(PropertiesHelper.SETTINGS_CHAT_ID);
      engine.tickPlayer(chatId);
      return Response.ok()
            .build();
   }

   @GET
   @Path("/cancel")
   public Response tick() {
      if (engine != null) {
         engine.cancel();
      }
      return Response.ok("matchbot cancelled")
            .build();
   }

   @GET
   @Path("/teams")
   public TeamsView teams() {
      TeamsView tv = new TeamsView(ScoreBotConstants.TEAMS_TEMPLATE, teamsHelper.getProperties());
      return tv;
   }

   @POST
   @Path("/teams")
   public Response addTeam(@FormParam("teamId") String teamId, @FormParam("teamName") String teamName) {
      boolean validId = validateId(teamId);
      boolean validName = validateName(teamName);
      if (!validId || !validName) {
         return Response.status(Status.BAD_REQUEST)
               .entity("no valid name or id entered <a href=\"/service/teams\">back</a>")
               .build();
      }
      try {
         teamsHelper.setProperty(teamId, teamName);
      } catch (IOException e) {
         return Response.status(Status.BAD_REQUEST)
               .build();
      }

      return Response.ok("You've added a new team! <a href=\"/service/teams\">back</a>")
            .build();
      // try {
      //// URI uri = new URI("/service/teams");
      //// return Response.temporaryRedirect(uri).build();
      // } catch (URISyntaxException e) {
      // e.printStackTrace();
      // return Response.status(Status.INTERNAL_SERVER_ERROR).build();
      // }
   }

   private boolean validateName(String teamName) {
      if (teamName == null || teamName.isEmpty()) {
         return false;
      }
      return teamName.length() < 20;
   }

   private boolean validateId(String teamId) {
      if (teamId == null || teamId.isEmpty()) {
         return false;
      }
      return true;
   }

   private MatchesOverview matchesOverview;
   private HltvMatchesOverviewParser matchesParser;
}
