package de.cb.home.telegram.server.resources;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.cb.home.telegram.bot.ScoreBotEngine;

public class ScoreBotFacade {

	private static ScoreBotFacade instance;

	Map<String, Pair<ScoreBotEngine, List<String>>> runningBots;

	private ScoreBotFacade() {
		runningBots = new HashMap<String, Pair<ScoreBotEngine, List<String>>>();
	}

	public static ScoreBotFacade getInstance() {
		if (instance == null) {
			instance = new ScoreBotFacade();
		}
		return instance;
	}

	public void startTicker(String matchId, String chatId) {
		if (runningBots.get(matchId) == null) {
			Pair<ScoreBotEngine, List<String>> pair = new Pair<>();
//			runningBots.put(matchId, new Pair<ScoreBotEngine, List<String>>(new ScoreBotEngine(settingsHelper, teamsHelper), new ArrayList<String>()));
			runningBots.put(matchId, pair);
		}
	}

	public List<String> getChats(String matchId) {
		return runningBots.get(matchId).getValue();
	}
	

}
