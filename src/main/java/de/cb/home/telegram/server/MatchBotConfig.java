package de.cb.home.telegram.server;

import io.dropwizard.Configuration;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

public class MatchBotConfig extends Configuration {

	@NotEmpty
	private String template;

	@NotEmpty
	private String settingsPath;

	@NotEmpty
	private String teamsPath;

	@NotEmpty
	private String chatsPath;

	@NotEmpty
	private String defaultName = "Matchbot";

	@JsonProperty
	public String getTemplate() {
		return template;
	}

	@JsonProperty
	public void setTemplate(String template) {
		this.template = template;
	}

	@JsonProperty
	public String getDefaultName() {
		return defaultName;
	}

	@JsonProperty
	public void setDefaultName(String name) {
		this.defaultName = name;
	}

	@JsonProperty
	public String getSettingsPath() {
		return settingsPath;
	}

	@JsonProperty
	public void setSettingsPath(String settingsPath) {
		this.settingsPath = settingsPath;
	}

	@JsonProperty
	public String getTeamsPath() {
		return teamsPath;
	}

	@JsonProperty
	public void setTeamsPath(String teamsPath) {
		this.teamsPath = teamsPath;
	}

	@JsonProperty
	public String getChatsPath() {
		return chatsPath;
	}

	@JsonProperty
	public void setChatsPath(String chatsPath) {
		this.chatsPath = chatsPath;
	}

}
