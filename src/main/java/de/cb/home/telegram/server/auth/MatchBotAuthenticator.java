package de.cb.home.telegram.server.auth;

import com.google.common.base.Optional;

import io.dropwizard.auth.AuthenticationException;
import io.dropwizard.auth.Authenticator;
import io.dropwizard.auth.basic.BasicCredentials;

public class MatchBotAuthenticator implements Authenticator<BasicCredentials, SimplePrincipal> {
	@Override
	public Optional<SimplePrincipal> authenticate(BasicCredentials credentials) throws AuthenticationException {
		if ("spartans".equals(credentials.getPassword())) {
			return Optional.of(new SimplePrincipal(credentials.getUsername()));
		}
		return Optional.absent();
	}
}