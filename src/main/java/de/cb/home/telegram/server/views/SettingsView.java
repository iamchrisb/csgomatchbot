package de.cb.home.telegram.server.views;

import java.util.Map;

import de.cb.home.telegram.server.settings.Chat;
import io.dropwizard.views.View;

public class SettingsView extends View {

	private Map<String, Chat> chats;
	private String debugMode;

	public SettingsView(String templateName, Map<String, Chat> chats, String debugMode) {
		super(templateName);
		this.chats = chats;
		this.debugMode = debugMode;
	}

	public Map<String, Chat> getChats() {
		return chats;
	}

	public String getDebugMode() {
		return debugMode;
	}

}
