package de.cb.home.telegram.server.settings;

public class Chat {
	private String ownerOrTitle;
	private boolean active;
	private boolean group;

	public Chat() {
		//
	}

	public Chat(String ownerOrTitle, boolean active, final boolean group) {
		this.ownerOrTitle = ownerOrTitle;
		this.active = active;
		this.group = group;
	}

	public Chat(String ownerOrTitle, boolean active) {
		this.ownerOrTitle = ownerOrTitle;
		this.active = active;
		this.group = false;
	}

	public Chat(String ownerOrTitle) {
		this.ownerOrTitle = ownerOrTitle;
		this.active = false;
		this.group = false;
	}

	public String getOwnerOrTitle() {
		return ownerOrTitle;
	}

	public void setOwnerOrTitle(String owner) {
		this.ownerOrTitle = owner;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public boolean isGroup() {
		return group;
	}

	public void setGroup(boolean group) {
		this.group = group;
	}
}
