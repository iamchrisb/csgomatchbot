package de.cb.home.telegram.server;

import de.cb.home.telegram.bot.common.PropertiesHelper;
import de.cb.home.telegram.bot.common.ScoreBotConstants;
import de.cb.home.telegram.server.resources.MatchBotResource;
import de.cb.home.telegram.server.resources.TelegramResource;
import de.cb.home.telegram.server.resources.admin.SettingsResource;
import de.cb.home.telegram.server.settings.ChatSettingsHelper;
import de.cb.home.telegram.util.NgrokService;
import de.cb.home.telegram.util.model.Tunnel;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.lifecycle.ServerLifecycleListener;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;

import java.io.IOException;

import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.TelegramBotAdapter;
import com.pengrad.telegrambot.response.SetWebhookResponse;

public class MatchBotApplication extends Application<MatchBotConfig> {

   private static final String APPLICATION_PORT_NAME = "application";

   public static void main(String[] args) throws Exception {
      new MatchBotApplication().run(args);
   }

   protected int applicationPort;
   protected int adminPort;

   @Override
   public void run(MatchBotConfig configuration, Environment environment) throws Exception {

      ChatSettingsHelper chatsHelper = new ChatSettingsHelper(configuration.getChatsPath());
      PropertiesHelper settingsHelper = new PropertiesHelper(configuration.getSettingsPath());
      PropertiesHelper teamsHelper = new PropertiesHelper(configuration.getTeamsPath());

      MatchBotResource matchBotRes = new MatchBotResource(configuration, settingsHelper, teamsHelper, chatsHelper);
      SettingsResource settingsRes = new SettingsResource(configuration, chatsHelper, settingsHelper);
      TelegramResource telegramRes = new TelegramResource();

      environment.jersey()
            .register(matchBotRes);
      environment.jersey()
            .register(settingsRes);
      environment.jersey()
            .register(telegramRes);
      // environment.jersey()
      // .register(new BasicAuthProvider<SimplePrincipal>(new
      // MatchBotAuthenticator(), "SUPER SECRET STUFF"));

      environment.lifecycle()
            .addServerLifecycleListener(new ServerLifecycleListener() {
               @Override
               public void serverStarted(Server server) {
                  Connector[] connectors = server.getConnectors();
                  for (Connector connector : connectors) {
                     int localPort = ((ServerConnector) connector).getLocalPort();
                     String name = ((ServerConnector) connector).getName();
                     if (name.trim()
                           .toLowerCase()
                           .equals(APPLICATION_PORT_NAME)) {
                        ServerConstants.APPLICATION_PORT = localPort;
                     } else {
                        ServerConstants.ADMIN_PORT = localPort;
                     }
                  }

                  try {
                     Tunnel httpsTunnel = NgrokService.getCurrentHttpsTunnelForPort(String
                           .valueOf(ServerConstants.APPLICATION_PORT));
                     if (httpsTunnel == null) {
                        throw new RuntimeException(
                              "Ngrok service could not be found, pls start ngrok using: \"ngrok http "
                                    + ServerConstants.APPLICATION_PORT + "\"");
                     }
                     String publicUrl = httpsTunnel.getPublic_url();

                     TelegramBot bot = TelegramBotAdapter.build(ScoreBotConstants.BOT_TOKEN);
                     String newWebHookUrl = getWebhookUrl(publicUrl);
                     System.out.println(newWebHookUrl);

                     // SetWebhookResponse response =
                     // bot.setWebhook(newWebHookUrl);

                     bot.sendMessage(ScoreBotConstants.CHAT_ID_ME, newWebHookUrl);
                     // getResponseText(response, newWebHookUrl));

                  } catch (IOException e) {
                     e.printStackTrace();
                  }
               }

               private String getWebhookUrl(String publicUrl) {
                  return publicUrl + "/service/" + ScoreBotConstants.HOME_SITE;
               }

               private String getResponseText(SetWebhookResponse response, String webhookUrl) {
                  StringBuilder sb = new StringBuilder();
                  sb.append("Description: " + response.description() + "\n");
                  sb.append("Webhook url: " + webhookUrl);
                  return sb.toString();
               }
            });

   }

   @Override
   public void initialize(Bootstrap<MatchBotConfig> bootstrap) {
      bootstrap.addBundle(new ViewBundle<MatchBotConfig>());
      bootstrap.addBundle(new AssetsBundle("/assets/", "/"));
   }

   @Override
   public String getName() {
      return "matchbot";
   }

}
