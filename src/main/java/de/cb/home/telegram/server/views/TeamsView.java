package de.cb.home.telegram.server.views;

import java.util.Map;

import io.dropwizard.views.View;

public class TeamsView extends View {

	private Map<Object, Object> teams;

	public TeamsView(String templateName, Map<Object, Object> teams) {
		super(templateName);
		this.teams = teams;
	}

	public Map<Object, Object> getTeams() {
		return teams;
	}
}
