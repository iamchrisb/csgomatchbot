package de.cb.home.telegram.server.resources;

import java.io.FileNotFoundException;

import de.cb.home.telegram.bot.common.PropertiesHelper;
import de.cb.home.telegram.server.MatchBotConfig;

public class ApplicationFacade {

	private PropertiesHelper settingsHelper;
	private PropertiesHelper teamsHelper;
	private ScoreBotFacade scoreBotFacade;
	private MatchBotConfig configuration;

	public ApplicationFacade(MatchBotConfig configuration) throws FileNotFoundException {
		this.configuration = configuration;
		settingsHelper = new PropertiesHelper(configuration.getSettingsPath());
		teamsHelper = new PropertiesHelper(configuration.getTeamsPath());
	}

}
