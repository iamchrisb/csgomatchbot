package de.cb.home.telegram.server.settings;

import java.io.IOException;

public class TestChat {
	public static void main(String[] args) throws IOException {
		ChatSettingsHelper chatHelper = new ChatSettingsHelper("/var/www/csgomatchbot/chats.csmb");
		try {
			chatHelper.setChat("-140382869", new Chat("Ticker"));
			chatHelper.setChat("207140128", new Chat("Ban"));
			chatHelper.setChat("190957563", new Chat("Chris"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
