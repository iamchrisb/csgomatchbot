package de.cb.home.telegram.server.auth;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

public class SimplePrincipal implements Principal {

	private String name;

	public SimplePrincipal(String name) {
		this.name = name;
	}

	private List<String> roles = new ArrayList<>();

	public List<String> getRoles() {
		return roles;
	}

	public boolean isUserInRole(String roleToCheck) {
		return roles.contains(roleToCheck);
	}

	@Override
	public String getName() {
		return this.name;
	}
}
