package de.cb.home.telegram.util.model;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.Value;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Tunnels {
	private String uri;
	private List<Tunnel> tunnels;
}
