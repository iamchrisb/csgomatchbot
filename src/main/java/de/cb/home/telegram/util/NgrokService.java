package de.cb.home.telegram.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.TelegramBotAdapter;

import de.cb.home.telegram.bot.common.ScoreBotConstants;
import de.cb.home.telegram.util.model.Tunnel;
import de.cb.home.telegram.util.model.Tunnels;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class NgrokService {

	private static final String HTTPS = "https";
	private static final String HTTP = "http";
	public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
	private static final String URL = "http://localhost:4040/api/tunnels";

	static public Tunnel getCurrentHttpTunnelForPort(String port) throws IOException {
		List<Tunnel> currentTunnels = getCurrentTunnels();
		for (Tunnel tunnel : currentTunnels) {
			if (tunnel.getConfig().getAddr().contains(port) && tunnel.getProto().equals(HTTP)) {
				return tunnel;
			}
		}
		return null;
	}

	static public Tunnel getCurrentHttpsTunnelForPort(String port) throws IOException {
		List<Tunnel> currentTunnels = getCurrentTunnels();
		for (Tunnel tunnel : currentTunnels) {
			if (tunnel.getConfig().getAddr().contains(port) && tunnel.getProto().equals(HTTPS)) {
				return tunnel;
			}
		}
		return null;
	}

	private static List<Tunnel> getCurrentTunnels() throws IOException {
		OkHttpClient client = new OkHttpClient();

		Request request = new Request.Builder().url(URL).build();

		Response response = client.newCall(request).execute();

		String jsonString = response.body().string();

		ObjectMapper mapper = new ObjectMapper();
		Tunnels tunnels = mapper.readValue(jsonString, Tunnels.class);
		return tunnels.getTunnels();
	}

	public static void main(String[] args) throws IOException {
		System.out.println(ScoreBotConstants.TELEGRAM_UPDATES_URL);

		TelegramBot telegramBot = TelegramBotAdapter.build(ScoreBotConstants.BOT_TOKEN);
//		telegramBot.sendMessage(ScoreBotConstants.TICKER_GROUP_ID,
//				"Lul, was willst du Made?");
		// telegramBot.setWebhook("https://cbb69ca4.ngrok.io/service/telegram");
	}

}
