package de.cb.home.telegram.util;

public class ChatService {

	private static ChatService instance;

	public static ChatService getInstance() {
		if (instance == null) {
			instance = new ChatService();
		}
		return instance;
	}
}
