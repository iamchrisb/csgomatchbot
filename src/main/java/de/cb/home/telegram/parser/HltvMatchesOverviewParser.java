package de.cb.home.telegram.parser;

import de.cb.home.telegram.bot.common.ScoreBotConstants;
import de.cb.home.telegram.parser.hltv.Match;
import de.cb.home.telegram.parser.hltv.MatchDay;
import de.cb.home.telegram.parser.hltv.MatchesOverview;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HltvMatchesOverviewParser {

   public MatchesOverview fetchLiveMatchesFromHTML() {
      Document document;

      try {
         document = Jsoup.connect(ScoreBotConstants.HLTV_MATCHES_DOMAIN)
               .userAgent(ScoreBotConstants.USER_AGENT_VALUE)
               .get();
      } catch (IOException e) {
         e.printStackTrace();
         return null;
      }

      if (document == null) {
         return null;
      }

      MatchesOverview matchesOverview = MatchesOverview.builder()
            .build();

      List<MatchDay> matchDays = new LinkedList<>();
      List<Match> matches = new LinkedList<>();

      final Element upcomingMatchesHTML = document.getElementsByClass("upcoming-matches")
            .first();

      final Elements matchDaysHTML = upcomingMatchesHTML.getElementsByClass("match-day");

      /**
       * match-days part begins here
       */
      for (Element currentMatchDayHTML : matchDaysHTML) {

         // stop after two days
         if (matchDays.size() == 2) {
            break;
         }

         final Element matchDayDateHTML = currentMatchDayHTML.getElementsByClass("standard-headline")
               .first();

         final String matchDayDateString = matchDayDateHTML.html();

         final MatchDay matchDay = MatchDay.builder()
               .date(matchDayDateString)
               .build();

         final Elements listOfMatchesHTML = currentMatchDayHTML.getElementsByClass("upcoming-match");

         /**
          * match part begins here
          */
         for (Element currentMatchHTML : listOfMatchesHTML) {

            final Match currentMatch = Match.builder()
                  .build();

            final String matchLinkWithIdHTML = currentMatchHTML.attr("href");
            String currentMatchId = parseMatchId(matchLinkWithIdHTML);

            currentMatch.setMatchId(currentMatchId);

            final Element timeHTML = currentMatchHTML.getElementsByAttribute("data-time-format")
                  .first();

            if (timeHTML == null) {
               // ignore if time is missing
               continue;
            }

            currentMatch.setStartTime(timeHTML.html());

            final Elements teamCellsHTML = currentMatchHTML.getElementsByClass("team-cell");

            for (int i = 0; i < teamCellsHTML.size(); i++) {
               final Element currentTeamCell = teamCellsHTML.get(i);

               final Element currentTeam = currentTeamCell
                     .getElementsByClass("team")
                     .first();

               //               final String currentTeamLogoHref = currentTeamCell.getElementsByClass("logo")
               //                     .first()
               //                     .attr("src");

               if (i == 0) {
                  currentMatch.setTeamNameA(currentTeam.html());
                  //                  currentMatch.setTeamALogoUrl(currentTeamLogoHref);
               } else {
                  currentMatch.setTeamNameB(currentTeam.html());
                  //                  currentMatch.setTeamALogoUrl(currentTeamLogoHref);
               }
            }

            final Element starCellHTML = currentMatchHTML.getElementsByClass("star-cell")
                  .first();

            if (starCellHTML == null) {
               // some matches are already in the list without any specifications but startTime and date, so go on, if there is something null
               continue;
            }

            final Element mapTextHTML = starCellHTML.getElementsByClass("map-text")
                  .first();

            currentMatch.setMode(mapTextHTML.html());

            final Element eventInformationHtml = currentMatchHTML.getElementsByClass("event")
                  .first();

            final String eventName = eventInformationHtml.getElementsByClass("event-name")
                  .first()
                  .html();

            //            final String eventLogo = eventInformationHtml.getElementsByClass("event-logo")
            //                  .first()
            //                  .attr("src");

            currentMatch.setEventName(eventName);
            //            currentMatch.setEventLogoHref(eventLogo);

            matches.add(currentMatch);
         }

         matchDay.setMatches(matches);
         matchDays.add(matchDay);
      }

      matchesOverview.setMatchDays(matchDays);

      return matchesOverview;
   }

   private String parseMatchId(final String matchLinkWithIdHTML) {
      return matchLinkWithIdHTML.split("/")[2];
   }
}
