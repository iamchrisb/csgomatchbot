package de.cb.home.telegram.parser.hltv;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@Builder
@ToString
public class MatchDay {
   private String date;
   private List<Match> matches;
}
