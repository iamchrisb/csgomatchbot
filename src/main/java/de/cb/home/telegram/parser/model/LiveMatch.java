package de.cb.home.telegram.parser.model;

public class LiveMatch {
	private String matchId;
	private String teamNameA;
	private String teamNameB;
	private String time;
	private String teamALogoUrl;
	private String teamBLogoUrl;

	public LiveMatch(String matchId, String teamNameA, String teamNameB, String time) {
		this.matchId = matchId;
		this.teamNameA = teamNameA;
		this.teamNameB = teamNameB;
		this.time = time;
	}

	public String getMatchId() {
		return matchId;
	}

	public void setMatchId(String matchId) {
		this.matchId = matchId;
	}

	public String getTeamNameA() {
		return teamNameA;
	}

	public void setTeamNameA(String teamNameA) {
		this.teamNameA = teamNameA;
	}

	public String getTeamNameB() {
		return teamNameB;
	}

	public void setTeamNameB(String teamNameB) {
		this.teamNameB = teamNameB;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getTeamALogoUrl() {
		return teamALogoUrl;
	}

	public void setTeamALogoUrl(String teamALogoUrl) {
		this.teamALogoUrl = teamALogoUrl;
	}

	public String getTeamBLogoUrl() {
		return teamBLogoUrl;
	}

	public void setTeamBLogoUrl(String teamBLogoUrl) {
		this.teamBLogoUrl = teamBLogoUrl;
	}

	@Override
	public String toString() {
		return "LiveMatch [matchId=" + matchId + ", teamNameA=" + teamNameA + ", teamNameB=" + teamNameB + ", startTime="
				+ time + "]";
	}

}
