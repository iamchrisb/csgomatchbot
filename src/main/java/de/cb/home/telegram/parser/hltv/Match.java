package de.cb.home.telegram.parser.hltv;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Builder
public class Match {
   private String startTime;
   private String matchId;
   private String teamNameA;
   private String teamNameB;
   private String teamALogoUrl;
   private String teamBLogoUrl;

   private String eventName;
   private String eventLogoHref;

   private String mode;
}
