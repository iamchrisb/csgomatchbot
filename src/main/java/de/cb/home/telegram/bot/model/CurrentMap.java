package de.cb.home.telegram.bot.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.cb.home.telegram.bot.model.score.LiveLog;
import de.cb.home.telegram.bot.model.score.MapPart;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CurrentMap {
	private boolean live;
	private String map;
	private int currentCtScore;
	private int currentTScore;
	private int currentCTTeam;
	private int currentTTeam;
	private int mapOrdinal;
	private LiveLog liveLog;

	private MapPart secondHalf;
	private MapPart overtime;
	private MapPart firstHalf;

	public CurrentMap() {
		// TODO Auto-generated constructor stub
	}

	public boolean isLive() {
		return live;
	}

	public void setLive(boolean live) {
		this.live = live;
	}

	public String getMap() {
		return map;
	}

	public void setMap(String map) {
		this.map = map;
	}

	public int getCurrentCtScore() {
		return currentCtScore;
	}

	public void setCurrentCtScore(int currentCtScore) {
		this.currentCtScore = currentCtScore;
	}

	public int getCurrentTScore() {
		return currentTScore;
	}

	public void setCurrentTScore(int currentTScore) {
		this.currentTScore = currentTScore;
	}

	public int getCurrentCTTeam() {
		return currentCTTeam;
	}

	public void setCurrentCTTeam(int currentCTTeam) {
		this.currentCTTeam = currentCTTeam;
	}

	public int getCurrentTTeam() {
		return currentTTeam;
	}

	public void setCurrentTTeam(int currentTTeam) {
		this.currentTTeam = currentTTeam;
	}

	public int getMapOrdinal() {
		return mapOrdinal;
	}

	public void setMapOrdinal(int mapOrdinal) {
		this.mapOrdinal = mapOrdinal;
	}

	public LiveLog getLiveLog() {
		return liveLog;
	}

	public void setLiveLog(LiveLog liveLog) {
		this.liveLog = liveLog;
	}

	public MapPart getSecondHalf() {
		return secondHalf;
	}

	public void setSecondHalf(MapPart secondHalf) {
		this.secondHalf = secondHalf;
	}

	public MapPart getOvertime() {
		return overtime;
	}

	public void setOvertime(MapPart overtime) {
		this.overtime = overtime;
	}

	public MapPart getFirstHalf() {
		return firstHalf;
	}

	public void setFirstHalf(MapPart firstHalf) {
		this.firstHalf = firstHalf;
	}
}
