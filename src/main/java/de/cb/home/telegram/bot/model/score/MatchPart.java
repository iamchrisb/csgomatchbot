package de.cb.home.telegram.bot.model.score;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class MatchPart {

	private MapPart firstHalf;
	private MapPart secondHalf;
	private MapPart overtime;
	private String map;
	private Map<String, String> scores;

	public MatchPart() {
		// TODO Auto-generated constructor stub
	}

	public MapPart getFirstHalf() {
		return firstHalf;
	}

	public void setFirstHalf(MapPart firstHalf) {
		this.firstHalf = firstHalf;
	}

	public MapPart getSecondHalf() {
		return secondHalf;
	}

	public void setSecondHalf(MapPart secondHalf) {
		this.secondHalf = secondHalf;
	}

	public MapPart getOvertime() {
		return overtime;
	}

	public void setOvertime(MapPart overtime) {
		this.overtime = overtime;
	}

	public String getMap() {
		return map;
	}

	public void setMap(String map) {
		this.map = map;
	}

	public Map<String, String> getScores() {
		return scores;
	}

	public void setScores(Map<String, String> scores) {
		this.scores = scores;
	}

}
