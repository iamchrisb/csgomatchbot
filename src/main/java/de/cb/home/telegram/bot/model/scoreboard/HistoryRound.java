package de.cb.home.telegram.bot.model.scoreboard;

public class HistoryRound {
	private int roundOrdinal;
	private String type;

	public HistoryRound() {
		// TODO Auto-generated constructor stub
	}

	public int getRoundOrdinal() {
		return roundOrdinal;
	}

	public void setRoundOrdinal(int roundOrdinal) {
		this.roundOrdinal = roundOrdinal;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
