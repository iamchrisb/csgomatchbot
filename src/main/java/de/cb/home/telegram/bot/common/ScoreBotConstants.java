package de.cb.home.telegram.bot.common;

public interface ScoreBotConstants {

   // https://api.telegram.org/bot213866101:AAEAJUTWKqbyGB5C_CtUxXcmDkZr4hfXloM/getUpdates

   public static final String USER_AGENT_VALUE = "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2";
   public static final String USER_AGENT_KEY = "http.agent";

   public static final String HLTV_DOMAIN = "http://www.hltv.org";
   public static final String HLTV_MATCHES_DOMAIN = HLTV_DOMAIN + "/matches";

   static final String BOT_TOKEN = "213866101:AAEAJUTWKqbyGB5C_CtUxXcmDkZr4hfXloM";
   static final String TELEGRAM_BOT_URL = "https://api.telegram.org/bot" + BOT_TOKEN;
   static final String TELEGRAM_UPDATES_URL = TELEGRAM_BOT_URL + "/" + "getUpdates";

   static final String SCORE_BOT_URL = "http://scorebot2.hltv.org";
   static final String SCORE_BOT_PORT = "10022";

   static final String CHAT_ID = "-27521194";
   static final String CHAT_SPAM_ID = "-122537574";
   static final String CHAT_ID_ME = "190957563";
   static final String TICKER_GROUP_ID = "-140382869";

   /**
    * templates
    */
   static final String TEAMS_TEMPLATE = "teams.ftl";
   static final String HLTV_MATCHES_TEMPLATE = "matches.ftl";
   static final String SETTINGS_TEMPLATE = "settings.ftl";

   /**
    * paths
    */
   static final String SETTINGS_PATH = "settings";
   static final String TELEGRAM_PATH = "telegram";

   static final String HOME_SITE = "matches";
}
