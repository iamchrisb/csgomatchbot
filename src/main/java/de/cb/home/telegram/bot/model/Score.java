package de.cb.home.telegram.bot.model;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import de.cb.home.telegram.bot.model.score.MatchPart;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Score {
	private Long listId;
	private boolean matchLive;
	private CurrentMap currentMap;
	private Map<String, MatchPart> mapScores;

	public Score() {
		// TODO Auto-generated constructor stub
	}

	public Score(CurrentMap currentMap) {
		this.currentMap = currentMap;
	}

	public CurrentMap getCurrentMap() {
		return currentMap;
	}

	public void setCurrentMap(CurrentMap currentMap) {
		this.currentMap = currentMap;
	}

	public Long getListId() {
		return listId;
	}

	public void setListId(Long listId) {
		this.listId = listId;
	}

	public boolean isMatchLive() {
		return matchLive;
	}

	public void setMatchLive(boolean matchLive) {
		this.matchLive = matchLive;
	}

	public Map<String, MatchPart> getMapScores() {
		return mapScores;
	}

	public void setMapScores(Map<String, MatchPart> mapScores) {
		this.mapScores = mapScores;
	}
}
