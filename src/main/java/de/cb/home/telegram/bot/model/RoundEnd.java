package de.cb.home.telegram.bot.model;

public class RoundEnd {
	
	public static final String JSON_KEY = "RoundEnd";

	private int counterTerroristScore;
	private int terroristScore;
	private String winner;
	private String winType;

	public RoundEnd(int counterTerroristScore, int terroristScore, String winner, String winType) {
		this.counterTerroristScore = counterTerroristScore;
		this.terroristScore = terroristScore;
		this.winner = winner;
		this.winType = winType;
	}

	public RoundEnd() {
		// TODO Auto-generated constructor stub
	}

	public int getCounterTerroristScore() {
		return counterTerroristScore;
	}

	public void setCounterTerroristScore(int counterTerroristScore) {
		this.counterTerroristScore = counterTerroristScore;
	}

	public int getTerroristScore() {
		return terroristScore;
	}

	public void setTerroristScore(int terroristScore) {
		this.terroristScore = terroristScore;
	}

	public String getWinner() {
		return winner;
	}

	public void setWinner(String winner) {
		this.winner = winner;
	}

	public String getWinType() {
		return winType;
	}

	public void setWinType(String winType) {
		this.winType = winType;
	}

}
