package de.cb.home.telegram.bot.model.scoreboard;

public class MatchHistory {

	private int currentRound;
	private HistoryRound[] firstHalf;
	private HistoryRound[] secondHalf;

	public MatchHistory() {
		// TODO Auto-generated constructor stub
	}

	public int getCurrentRound() {
		return currentRound;
	}

	public void setCurrentRound(int currentRound) {
		this.currentRound = currentRound;
	}

	public HistoryRound[] getFirstHalf() {
		return firstHalf;
	}

	public void setFirstHalf(HistoryRound[] firstHalf) {
		this.firstHalf = firstHalf;
	}

	public HistoryRound[] getSecondHalf() {
		return secondHalf;
	}

	public void setSecondHalf(HistoryRound[] secondHalf) {
		this.secondHalf = secondHalf;
	}
}
