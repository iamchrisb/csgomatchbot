package de.cb.home.telegram.bot;

import de.cb.home.telegram.bot.common.PropertiesHelper;
import de.cb.home.telegram.bot.common.ScoreBotConstants;
import de.cb.home.telegram.bot.common.ScoreBotHelper;
import de.cb.home.telegram.bot.model.Kill;
import de.cb.home.telegram.bot.model.RoundEnd;
import de.cb.home.telegram.bot.model.Score;
import de.cb.home.telegram.bot.model.ScoreBoard;
import de.cb.home.telegram.server.resources.ScoreBotFacade;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import io.socket.emitter.Emitter.Listener;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.TelegramBotAdapter;

public class ScoreBotEngine {

   public static Logger LOGGER = LoggerFactory.getLogger(ScoreBotEngine.class);

   private static final String LOG_JSON_KEY = "log";
   private static final String READY_FOR_MATCH = "readyForMatch";

   private static TelegramBot bot;
   private static ObjectMapper objMapper;

   private boolean scoreHasNotBeenPosted = false;

   private static String SOCKET_URL = ScoreBotConstants.SCORE_BOT_URL + ":" + ScoreBotConstants.SCORE_BOT_PORT;
   private Socket socket;

   private PropertiesHelper settingsHelper;
   private PropertiesHelper teamsHelper;

   JSONParser parser;

   private ScoreBoard scoreBoard;

   private Score score;

   private RoundEnd currentLastRound;

   private int currentLastRountCount;

   private boolean hasNextRound;

   private String matchId;

   public ScoreBotEngine(PropertiesHelper settingsHelper, PropertiesHelper teamsHelper) throws URISyntaxException {
      this.settingsHelper = settingsHelper;
      this.teamsHelper = teamsHelper;

      parser = new JSONParser();

      bot = TelegramBotAdapter.build(ScoreBotConstants.BOT_TOKEN);
      objMapper = new ObjectMapper();
      socket = IO.socket(SOCKET_URL);
   }

   public void reset() {
      tryToDisconnect();
      scoreHasNotBeenPosted = false;
      scoreBoard = null;
   }

   private void tryToDisconnect() {
      try {
         socket.disconnect();
      } catch (Exception e) {
         e.printStackTrace();
      }
   }

   public void update(final String matchId) {
      this.matchId = matchId;

      LOGGER.info("#### INFO: Start ticking with SocketIO...");
      socket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
         public void call(Object... arg0) {
            LOGGER.info("#### INFO: Connected with: " + SOCKET_URL);
            bot.sendMessage(ScoreBotConstants.CHAT_ID_ME, "Connected with: " + SOCKET_URL);
         }
      })
            .on(Socket.EVENT_CONNECT_ERROR, new Listener() {
               @Override
               public void call(Object... arg0) {
                  Exception e = (Exception) arg0[0];
                  e.printStackTrace();
                  LOGGER.error("##### ERROR: Connection failed! " + e.getMessage());
                  bot.sendMessage(ScoreBotConstants.CHAT_ID_ME, e.getMessage());
               }
            })
            .on(Events.SCORE.key(), new Listener() {
               public void call(Object... args) {
                  LOGGER.info("#### INFO: A wild score appeared!");
                  Object scoreObj = args[0];
                  LOGGER.info(scoreObj.toString());
                  // try {
                  // handleScoreEvent(scoreObj);
                  // } catch (IOException e) {
                  // e.printStackTrace();
                  // }
               }
            })
            .on(Events.LOG.key(), new Listener() {
               @Override
               public void call(Object... args) {
                  LOGGER.info("#### INFO: A wild log event appeared!");
                  Object logObj = args[0];
                  LOGGER.info(logObj.toString());
                  // try {
                  // handleScoreEvent(logObj);
                  // } catch (Exception e) {
                  // e.printStackTrace();
                  // }
               }
            })
            .on(Events.SCORE_BOARD.key(), new Listener() {
               @Override
               public void call(Object... args) {
                  LOGGER.info("#### INFO: A wild scoreboard event appeared!");
                  Object scoreBoardObj = args[0];
                  LOGGER.info(scoreBoardObj.toString());
                  // try {
                  // handleScoreBoardEvent(scoreBoardObj);
                  // } catch (IOException e) {
                  // e.printStackTrace();
                  // }
                  handleNewScoreEvent(scoreBoardObj);
               }
            });
      socket.emit(READY_FOR_MATCH, matchId);
      socket.connect();
   }

   private void handleNewScoreEvent(Object scoreObj) {
      try {
         scoreBoard = objMapper.readValue(scoreObj.toString(), ScoreBoard.class);
         boolean isNextRound = currentLastRountCount != scoreBoard.getCurrentRound();
         if (isNextRound) {
            bot.sendMessage(ScoreBotConstants.CHAT_ID_ME,
                  ScoreBotHelper.getScoreMessageByScoreBoard(scoreBoard));
            currentLastRountCount = scoreBoard.getCurrentRound();
         }
      } catch (IOException e) {
         e.printStackTrace();
         bot.sendMessage(ScoreBotConstants.CHAT_ID_ME, e.getMessage());
      }
   }

   private void handleScoreBoardEvent(Object scoreBoardObj)
         throws JsonParseException, JsonMappingException, IOException {
      System.out.println(scoreBoardObj);
      scoreBoard = objMapper.readValue(scoreBoardObj.toString(), ScoreBoard.class);

      // currentCTTeam = scoreBoard.getCtTeamName();
      // currentTTeam = scoreBoard.getTerroristTeamName();
      // currentMap = scoreBoard.getMapName();

      if (!scoreHasNotBeenPosted) {
         if (!getDebugProperty()) {
            sendMessageToAllChats();
            scoreHasNotBeenPosted = true;
         }
      }

      if (!getDebugProperty() && hasNextRound && currentLastRound != null) {
         String roundEndMessage = ScoreBotHelper.getRoundEndMessage(currentLastRound.getWinner(),
               scoreBoard.getCounterTerroristScore(), scoreBoard.getTerroristScore(), scoreBoard.getCtTeamName(),
               scoreBoard.getTerroristTeamName(), scoreBoard.getMapName());
         List<String> chats = ScoreBotFacade.getInstance()
               .getChats(this.matchId);
         for (String chatId : chats) {
            bot.sendMessage(chatId, roundEndMessage);
         }
         hasNextRound = false;
      }
   }

   private void sendMessageToAllChats() {
      List<String> chats = ScoreBotFacade.getInstance()
            .getChats(this.matchId);
      for (String chatId : chats) {
         // TODO check if chat is active or not
         bot.sendMessage(chatId, ScoreBotHelper.getScoreMessageByScoreBoard(scoreBoard));
      }
   }

   private void handleScoreEvent(Object scoreObj) throws JsonParseException, JsonMappingException, IOException {
      System.out.println(scoreObj);
      score = objMapper.readValue(scoreObj.toString(), Score.class);

      if (score == null) {
         LOGGER.error("Something is wrong, the initial score is null");
         return;
      }

      if (score.getListId() == -1) {
         scoreHasNotBeenPosted = true;
         List<String> chats = ScoreBotFacade.getInstance()
               .getChats(this.matchId);
         for (String chatId : chats) {
            handleMatchNotStarted(chatId);
         }
      }

      String ctTeam = teamsHelper.getProperty(score.getCurrentMap()
            .getCurrentCTTeam());
      String tTeam = teamsHelper.getProperty(score.getCurrentMap()
            .getCurrentTTeam());

      if (ctTeam != null && tTeam != null) {
         if (!getDebugProperty()) {
            List<String> chats = ScoreBotFacade.getInstance()
                  .getChats(this.matchId);
            for (String chatId : chats) {
               bot.sendMessage(chatId, ScoreBotHelper.getScoreMessage(score, teamsHelper));
            }
            scoreHasNotBeenPosted = true;
         }
      }
   }

   private void handleMatchNotStarted(final String chatId) {
      if (!getDebugProperty()) {
         bot.sendMessage(chatId, ScoreBotHelper.getMatchNotStartedMessage());
      }
   }

   private Boolean getDebugProperty() {
      return Boolean.valueOf(settingsHelper.getProperty(PropertiesHelper.SETTINGS_DEBUG));
   }

   @SuppressWarnings("unused")
   private void handleLogEvent(Object logObj, String chatId)
         throws JsonParseException, JsonMappingException, IOException, ParseException {
      JSONObject parse = (JSONObject) parser.parse(logObj.toString());
      JSONArray logJson = (JSONArray) parse.get(LOG_JSON_KEY);

      System.out.println(logJson);

      // the score event with all roundends is logged at the
      // beginning thats why we question for the size
      if (logJson.toString()
            .contains(RoundEnd.JSON_KEY) && logJson.size() < 10) {
         handleRoundEnd(logJson, chatId);
      }

      // JSONObject killJson = (JSONObject) logJson.get(Kill.JSON_KEY);
      // if (killJson != null && !killJson.isEmpty()) {
      // handleKill(killJson);
      // }
   }

   public void handleRoundEnd(JSONArray logJson, final String chatId)
         throws JsonParseException, JsonMappingException, IOException {
      int size = logJson.size();
      for (int i = 0; i < size; i++) {
         JSONObject jsonObj = (JSONObject) logJson.get(i);
         JSONObject roundEndJson = (JSONObject) jsonObj.get(RoundEnd.JSON_KEY);

         if (roundEndJson == null || roundEndJson.isEmpty()) {
            continue;
         }

         currentLastRound = objMapper.readValue(roundEndJson.toString(), RoundEnd.class);
         hasNextRound = true;
      }

   }

   public void handleKill(JSONObject killJson, final String chatId) {
      System.out.println(killJson);
      try {
         Kill kill = objMapper.readValue(killJson.toString(), Kill.class);
         if (!getDebugProperty()) {
            bot.sendMessage(chatId, ScoreBotHelper.getKillMessage(kill));
         }
      } catch (IOException e) {
         e.printStackTrace();
      }
   }

   public void cancel() {
      try {
         reset();
      } catch (Exception e) {

      }
   }

   public ScoreBoard getScoreBoard() {
      return scoreBoard;
   }

   public Score getScore() {
      return score;
   }

   public void tickMaps(String chatId) {
      if (getDebugProperty()) {
         return;
      }

      if (score == null) {
         bot.sendMessage(chatId, "Please tick a match and ask me again.");
         return;
      }

      bot.sendMessage(chatId, ScoreBotHelper.getMapsMessage(score, teamsHelper));
   }

   public void tickPlayer(String chatId) {
      if (getDebugProperty()) {
         return;
      }

      if (score == null) {
         bot.sendMessage(chatId, "Please tick a match and ask me again.");
         return;
      }

      bot.sendMessage(chatId, ScoreBotHelper.getPlayerMessage(scoreBoard));
   }

}
