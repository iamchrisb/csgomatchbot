package de.cb.home.telegram.bot.model.score;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LiveLog {
	@JsonProperty("PlayersRequirement")
	private boolean playersRequirement;

	@JsonProperty("FiveKillsWhenEnemyElliminatedRequirement")
	private boolean fiveKillsWhenEnemyElliminatedRequirement;

	@JsonProperty("NotKnifeRoundRequirement")
	private boolean notKnifeRoundRequirement;

	@JsonProperty("BombInPlayRequirement")
	private boolean bombInPlayRequirement;

	@JsonProperty("RoundOneMaxPurchaseRequirement")
	private boolean roundOneMaxPurchaseRequirement;

	@JsonProperty("MatchStartRequirement")
	private boolean matchStartRequirement;

	@JsonProperty("MapNameRequirement")
	private boolean mapNameRequirement;

	@JsonProperty("FirstRoundOverRequirement")
	private boolean firstRoundOverRequirement;

	public LiveLog() {
		// TODO Auto-generated constructor stub
	}

	public boolean isPlayersRequirement() {
		return playersRequirement;
	}

	public void setPlayersRequirement(boolean playersRequirement) {
		this.playersRequirement = playersRequirement;
	}

	public boolean isFiveKillsWhenEnemyElliminatedRequirement() {
		return fiveKillsWhenEnemyElliminatedRequirement;
	}

	public void setFiveKillsWhenEnemyElliminatedRequirement(boolean fiveKillsWhenEnemyElliminatedRequirement) {
		this.fiveKillsWhenEnemyElliminatedRequirement = fiveKillsWhenEnemyElliminatedRequirement;
	}

	public boolean isNotKnifeRoundRequirement() {
		return notKnifeRoundRequirement;
	}

	public void setNotKnifeRoundRequirement(boolean notKnifeRoundRequirement) {
		this.notKnifeRoundRequirement = notKnifeRoundRequirement;
	}

	public boolean isBombInPlayRequirement() {
		return bombInPlayRequirement;
	}

	public void setBombInPlayRequirement(boolean bombInPlayRequirement) {
		this.bombInPlayRequirement = bombInPlayRequirement;
	}

	public boolean isRoundOneMaxPurchaseRequirement() {
		return roundOneMaxPurchaseRequirement;
	}

	public void setRoundOneMaxPurchaseRequirement(boolean roundOneMaxPurchaseRequirement) {
		this.roundOneMaxPurchaseRequirement = roundOneMaxPurchaseRequirement;
	}

	public boolean isMatchStartRequirement() {
		return matchStartRequirement;
	}

	public void setMatchStartRequirement(boolean matchStartRequirement) {
		this.matchStartRequirement = matchStartRequirement;
	}

	public boolean isMapNameRequirement() {
		return mapNameRequirement;
	}

	public void setMapNameRequirement(boolean mapNameRequirement) {
		this.mapNameRequirement = mapNameRequirement;
	}

	public boolean isFirstRoundOverRequirement() {
		return firstRoundOverRequirement;
	}

	public void setFirstRoundOverRequirement(boolean firstRoundOverRequirement) {
		this.firstRoundOverRequirement = firstRoundOverRequirement;
	}
}
