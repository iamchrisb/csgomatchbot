package de.cb.home.telegram.bot.model;

public class Kill {

	public static final String JSON_KEY = "Kill";

	private String killerName;
	private String killerSide;
	private String victimName;
	private String victimSide;
	private String weapon;
	private boolean headShot;

	public Kill(String killerName, String killerSide, String victimName, String victimSide, String weapon,
			boolean headShot) {
		this.killerName = killerName;
		this.killerSide = killerSide;
		this.victimName = victimName;
		this.victimSide = victimSide;
		this.weapon = weapon;
		this.headShot = headShot;
	}

	public Kill() {
		// TODO Auto-generated constructor stub
	}

	public String getKillerName() {
		return killerName;
	}

	public void setKillerName(String killerName) {
		this.killerName = killerName;
	}

	public String getKillerSide() {
		return killerSide;
	}

	public void setKillerSide(String killerSide) {
		this.killerSide = killerSide;
	}

	public String getVictimName() {
		return victimName;
	}

	public void setVictimName(String victimName) {
		this.victimName = victimName;
	}

	public String getVictimSide() {
		return victimSide;
	}

	public void setVictimSide(String victimSide) {
		this.victimSide = victimSide;
	}

	public String getWeapon() {
		return weapon;
	}

	public void setWeapon(String weapon) {
		this.weapon = weapon;
	}

	public boolean isHeadShot() {
		return headShot;
	}

	public void setHeadShot(boolean headShot) {
		this.headShot = headShot;
	}
}
