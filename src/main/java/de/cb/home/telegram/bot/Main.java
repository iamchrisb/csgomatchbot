package de.cb.home.telegram.bot;

import de.cb.home.telegram.parser.HltvMatchesOverviewParser;

import org.jsoup.nodes.Document;

public class Main {
   private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36";

   public static void main(String[] args) {
      String hltvUrl = "http://www.hltv.org/matches";
      Document document;
      //		try {
      //			document = Jsoup.connect(hltvUrl).userAgent(USER_AGENT).get();
      //			System.out.println(document);
      //		} catch (IOException e) {
      //			// TODO Auto-generated catch block
      //			e.printStackTrace();
      //		}

      final HltvMatchesOverviewParser hltvMatchesOverviewParser = new HltvMatchesOverviewParser();

      hltvMatchesOverviewParser.fetchLiveMatchesFromHTML();

   }
}
