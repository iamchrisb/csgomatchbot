package de.cb.home.telegram.bot;

public enum Events {
	// @formatter:off
	LOG("log"), SCORE("score"), SCORE_BOARD("scoreboard");
	// @formatter:on

	private String eventName;

	private Events(final String eventName) {
		this.eventName = eventName;
	}

	public String key() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
}
