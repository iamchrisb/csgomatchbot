package de.cb.home.telegram.bot.model.score;

public class MapPart {

	private int ctTeamDbId;
	private int ctScore;
	private int tTeamDbId;
	private int tScore;

	public MapPart() {
		// TODO Auto-generated constructor stub
	}

	public int getCtTeamDbId() {
		return ctTeamDbId;
	}

	public void setCtTeamDbId(int ctTeamDbId) {
		this.ctTeamDbId = ctTeamDbId;
	}

	public int getCtScore() {
		return ctScore;
	}

	public void setCtScore(int ctScore) {
		this.ctScore = ctScore;
	}

	public int gettTeamDbId() {
		return tTeamDbId;
	}

	public void settTeamDbId(int tTeamDbId) {
		this.tTeamDbId = tTeamDbId;
	}

	public int gettScore() {
		return tScore;
	}

	public void settScore(int tScore) {
		this.tScore = tScore;
	}

}
