package de.cb.home.telegram.bot.common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.Properties;

public class PropertiesHelper {

	public PropertiesHelper(final String propertiesPath) throws FileNotFoundException {
		this.propertiesPath = propertiesPath;
		read();
	}

	public static final String SETTINGS_CHAT_ID = "chatId";
	public static final String SETTINGS_DEBUG = "debug";
	Properties properties = new Properties();

	OutputStream output = null;
	InputStream input = null;

	private String propertiesPath;

	private void read() {
		try {
			input = new FileInputStream(propertiesPath);
			// load a properties file
			properties.load(input);
			// get the property value and print it out
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (input != null) {
				try {
					input.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public String getProperty(final Integer key) {
		return getProperty(key.toString());
	}

	public String getProperty(final String key) {
		read();
		return properties.getProperty(key);
	}

	public void setProperty(final String key, final String value) throws IOException {
		read();
		properties.setProperty(key, value);
		save();
	}

	private void save() throws FileNotFoundException, IOException {
		output = new FileOutputStream(propertiesPath);
		properties.store(output, "");
		output.close();
	}

	public Properties getProperties() {
		read();
		return this.properties;
	}

	public void putAll(Map<? extends Object, ? extends Object> table) throws IOException {
		read();
		properties.putAll(table);
		save();
	}

}
