package de.cb.home.telegram.bot.model.xml;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

public class Channel {

	@JacksonXmlProperty(isAttribute = true)
	private String title;
	@JacksonXmlProperty(isAttribute = true)
	private String description;
	@JacksonXmlProperty(isAttribute = true)
	private String link;

//	@JacksonXmlProperty(localName = "link:atom", isAttribute = false)
//	private String atomLink;

	@JacksonXmlProperty(localName = "item")
	@JacksonXmlElementWrapper(useWrapping = false)
	private Item[] items;

	public Channel() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public Item[] getItems() {
		return items;
	}

	public void setItems(Item[] items) {
		this.items = items;
	}

	@Override
	public String toString() {
		return "Channel [title=" + title + ", description=" + description + ", link=" + link + "]";
	}

}
