package de.cb.home.telegram.bot.model;

public class Suicide {
	private String playerName;
	private String side;

	public Suicide(String playerName, String side) {
		this.playerName = playerName;
		this.side = side;
	}

	public String getPlayerName() {
		return playerName;
	}

	public void setPlayerName(String playerName) {
		this.playerName = playerName;
	}

	public String getSide() {
		return side;
	}

	public void setSide(String side) {
		this.side = side;
	}
}
