package de.cb.home.telegram.bot.common;

import java.util.Map;

import de.cb.home.telegram.bot.model.Kill;
import de.cb.home.telegram.bot.model.Score;
import de.cb.home.telegram.bot.model.ScoreBoard;
import de.cb.home.telegram.bot.model.score.MatchPart;
import de.cb.home.telegram.bot.model.scoreboard.Player;

public class ScoreBotHelper {

	public static String getTelegrammedTeamName(String teamName) {
		if (teamName == null || teamName.isEmpty()) {
			return "unknown";
		}
		return teamName.replace(".", ":");
	}

	public static String getRoundEndMessage(final String roundWinner, final int ctScore, final int tScore,
			final String currentCTTeam, final String currentTTeam, final String currentMap) {
		StringBuilder msg = new StringBuilder();
		if (currentCTTeam != null && currentTTeam != null) {
			msg.append(ScoreBotHelper.getTelegrammedTeamName(currentCTTeam) + " vs "
					+ ScoreBotHelper.getTelegrammedTeamName(currentTTeam) + ", ");
		}
		msg.append("Round won by: " + roundWinner + ", score: CT (" + ctScore + ":" + tScore + ") T");

		if (currentMap != null) {
			msg.append(", on: " + currentMap);
		}
		return msg.toString();
	}

	public static String getScoreMessageByScoreBoard(ScoreBoard scoreBoard) {
		String telegrammedCTTeamName = getTelegrammedTeamName(scoreBoard.getCtTeamName());
		String telegrammedTTeamName = getTelegrammedTeamName(scoreBoard.getTerroristTeamName());
		return "Match: " + telegrammedCTTeamName + " vs " + telegrammedTTeamName + ", score: CT ("
				+ scoreBoard.getCounterTerroristScore() + ":" + scoreBoard.getTerroristScore() + ") T, map: "
				+ scoreBoard.getMapName();
	}

	public static String getScoreMessage(Score score, PropertiesHelper teamsHelper) {
		String ctTeam = ScoreBotHelper
				.getTelegrammedTeamName(teamsHelper.getProperty(score.getCurrentMap().getCurrentCTTeam()));
		String tTeam = ScoreBotHelper
				.getTelegrammedTeamName(teamsHelper.getProperty(score.getCurrentMap().getCurrentTTeam()));
		return "Match: " + ctTeam + " vs " + tTeam + ", score: CT (" + score.getCurrentMap().getCurrentCtScore() + ":"
				+ score.getCurrentMap().getCurrentTScore() + ") T, map: " + score.getCurrentMap().getMap();
	}

	public static String getKillMessage(Kill kill) {
		return kill.getKillerName() + " killed: " + kill.getVictimName() + " with: " + kill.getWeapon();
	}

	public static String getMatchNotStartedMessage() {
		return "This match hasn't started yet.";
	}

	public static String getMapsMessage(Score score, PropertiesHelper teamsHelper) {
		Map<String, MatchPart> mapScores = score.getMapScores();
		StringBuilder builder = new StringBuilder();

		if (mapScores.isEmpty() || mapScores.size() == 0) {
			return "No map data available";
		}

		for (Map.Entry<String, MatchPart> matchPart : mapScores.entrySet()) {
			builder.append(matchPart.getKey() + ": ");
			builder.append(matchPart.getValue().getMap() + ", ");
			for (Map.Entry<String, String> matchPartScore : matchPart.getValue().getScores().entrySet()) {
				String teamName = getTelegrammedTeamName(teamsHelper.getProperty(matchPartScore.getKey()));
				builder.append(teamName + " : " + matchPartScore.getValue());
				builder.append(" | ");
			}
			builder.append("\n");
		}
		return builder.toString();
	}

	public static String getPlayerMessage(ScoreBoard scoreBoard) {
		StringBuilder builder = new StringBuilder();
		if (scoreBoard.getTerrorists() == null || scoreBoard.getTerrorists().length == 0 || scoreBoard.getCt() == null
				|| scoreBoard.getCt().length == 0) {
			return "no player data available";
		}

		builder.append("Terrorists \n");
		for (Player currentPlayer : scoreBoard.getTerrorists()) {
			builder.append(
					currentPlayer.getName() + ": " + currentPlayer.getScore() + " | " + currentPlayer.getAssists()
							+ " | " + currentPlayer.getDeaths() + ", KD: " + currentPlayer.getRating());
			builder.append("\n");
		}

		builder.append("\n");
		builder.append("Counter Terrorists \n");
		for (Player currentPlayer : scoreBoard.getCt()) {
			builder.append(
					currentPlayer.getName() + ": " + currentPlayer.getScore() + " | " + currentPlayer.getAssists()
							+ " | " + currentPlayer.getDeaths() + ", KD: " + currentPlayer.getRating());
			builder.append("\n");
		}
		return builder.toString();
	}
}
