package de.cb.home.telegram.bot.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import de.cb.home.telegram.bot.model.scoreboard.MatchHistory;
import de.cb.home.telegram.bot.model.scoreboard.Player;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ScoreBoard {
	@JsonProperty("TERRORISTS")
	private Player[] terrorists;

	@JsonProperty("CT")
	private Player[] ct;

	private MatchHistory terroristMatchHistory;
	private MatchHistory ctMatchHistory;

	private boolean bombPlanted;
	private String mapName;
	private String terroristTeamName;
	private String ctTeamName;
	private int currentRound;
	private int counterTerroristScore;
	private int terroristScore;
	private int ctTeamId;
	private int tTeamId;

	public ScoreBoard() {
		// TODO Auto-generated constructor stub
	}

	public Player[] getTerrorists() {
		return terrorists;
	}

	public void setTerrorists(Player[] terrorists) {
		this.terrorists = terrorists;
	}

	public Player[] getCt() {
		return ct;
	}

	public void setCt(Player[] ct) {
		this.ct = ct;
	}

	public boolean isBombPlanted() {
		return bombPlanted;
	}

	public void setBombPlanted(boolean bombPlanted) {
		this.bombPlanted = bombPlanted;
	}

	public String getMapName() {
		return mapName;
	}

	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	public String getTerroristTeamName() {
		return terroristTeamName;
	}

	public void setTerroristTeamName(String terroristTeamName) {
		this.terroristTeamName = terroristTeamName;
	}

	public String getCtTeamName() {
		return ctTeamName;
	}

	public void setCtTeamName(String ctTeamName) {
		this.ctTeamName = ctTeamName;
	}

	public int getCurrentRound() {
		return currentRound;
	}

	public void setCurrentRound(int currentRound) {
		this.currentRound = currentRound;
	}

	public int getCounterTerroristScore() {
		return counterTerroristScore;
	}

	public void setCounterTerroristScore(int counterTerroristScore) {
		this.counterTerroristScore = counterTerroristScore;
	}

	public int getTerroristScore() {
		return terroristScore;
	}

	public void setTerroristScore(int terroristScore) {
		this.terroristScore = terroristScore;
	}

	public int getCtTeamId() {
		return ctTeamId;
	}

	public void setCtTeamId(int ctTeamId) {
		this.ctTeamId = ctTeamId;
	}

	public int gettTeamId() {
		return tTeamId;
	}

	public void settTeamId(int tTeamId) {
		this.tTeamId = tTeamId;
	}

	public MatchHistory getTerroristMatchHistory() {
		return terroristMatchHistory;
	}

	public void setTerroristMatchHistory(MatchHistory terroristMatchHistory) {
		this.terroristMatchHistory = terroristMatchHistory;
	}

	public MatchHistory getCtMatchHistory() {
		return ctMatchHistory;
	}

	public void setCtMatchHistory(MatchHistory ctMatchHistory) {
		this.ctMatchHistory = ctMatchHistory;
	}
}
