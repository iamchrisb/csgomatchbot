package de.cb.home.telegram.bot.model.xml;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Item {

	private static final String PLUS_SPLIT = "\\+";
	private String title;
	private String link;
	private String description;
	private String gametype;
	private String pubDate;

	DateTimeFormatter formatter = DateTimeFormat.forPattern("EEE, dd MMM yyyy HH:mm:ss Z");
	private DateTime pubDateTime;

	public Item() {
		// xml
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getGametype() {
		return gametype;
	}

	public void setGametype(String gametype) {
		this.gametype = gametype;
	}

	public String getPubDate() {
		return pubDate;
	}

	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
		pubDateTime = formatter.parseDateTime(pubDate);
	}

	public DateTime getPubDateTime() {
		return pubDateTime;
	}

	public void setPubDateTime(DateTime pubDateTime) {
		this.pubDateTime = pubDateTime;
	}

	public String getMatchId() {
		String string = link.split("-")[0];
		String[] split = string.split("/");
		return split[split.length - 1];
	}

	public String getFirstTeamName() {
		return title.split(" ")[0];
	}

	public String getSecondTeamName() {
		return title.split(" ")[2];
	}

	public String getTime() {
		// ["07","Mar","2016","20:00:00"]
		String[] timeAndDate = pubDate.split(",")[1].split(PLUS_SPLIT)[0].trim().split(" ");
		return timeAndDate[timeAndDate.length - 1];
	}

	public String getTimeStamp() {
		return String.valueOf(pubDateTime.getMillis());
	}

	public String getDay() {
		// ["07","Mar","2016","20:00:00"]
		String[] timeAndDate = pubDate.split(",")[1].split(PLUS_SPLIT)[0].trim().split(" ");
		return timeAndDate[0];
	}

	@Override
	public String toString() {
		return "Item [title=" + title + ", link=" + link + ", description=" + description + ", gametype=" + gametype
				+ ", pubDate=" + pubDate + "]";
	}
}
