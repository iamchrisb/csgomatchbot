<#-- type="de.cb.home.telegram.bot.views.TeamsView" -->
<html>
	<header>
		<link rel="stylesheet" href="/css/reset.css" type="text/css">
		<link rel="stylesheet" href="/css/furtive.min.css" type="text/css">
		<link rel="stylesheet" href="/css/styles.css" type="text/css">
		<script type="text/javascript" src="/libs/jquery.min.js"></script>
	</header>
    <body>
    	<div class="team-submit">
	    	<form action="/service/teams" role="form" class="my2" method="post">
	        	<label for="teamId">Team-ID</label>
	        	<input type="text" name="teamId" placeholder="Enter Team-ID">
		        <label for="teamName">Team Name</label>
		        <input type="text" name="teamName" placeholder="Enter Teamname">
		        <input type="submit" value="Add team" class="btn--blue">
	      	</form>
	    </div>
	    <div class="team-list">
	    	<h3> Teams </h3>
		    	<!-- <div class="grd"> -->
		    		<!--<div class="grd-row">-->
		    		<#list teams?keys as key>
		    		 	<div>${key} : ${teams[key]}</div>
					</#list>
					<!-- </div> -->
		    	<!-- </div> -->
	    </div>
	    <script>
	    	jQuery(document).ready(function() {
	    	});
	    </script>
    </body>
</html>