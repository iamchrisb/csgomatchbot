<#-- type="de.cb.home.telegram.bot.views.SettingsView" -->
<html>
	<header>
		<link rel="stylesheet" href="/css/reset.css" type="text/css">
		<link rel="stylesheet" href="/css/furtive.min.css" type="text/css">
		<link rel="stylesheet" href="/css/styles.css" type="text/css">
		<script type="text/javascript" src="/libs/jquery.min.js"></script>
	</header>
    <body>
	    <div class="chat-list">
	    	<h3> Choose a chat </h3>
		    	<!-- <div class="grd"> -->
		    		<!--<div class="grd-row">-->
		    		<#list chats?keys as key>
		    		 	<div class="entry">
		    		 		<div class="chatname">
								${chats[key].ownerOrTitle}  
								<#if chats[key].group>
									<input type="radio" id="mc" checked><label>Group</label>
								<#else>
									<input type="radio" id="mc"><label>Group</label>
								</#if>

								<#if chats[key].active >
									<input type="checkbox" id="active" checked> <label>Active</label>
								<#else>
									<input type="checkbox" id="active"> <label>Active</label>
								</#if>
							</div> 
							<span class="btn--blue choose-btn" data-chatid="${key}">choose</span>
							<span class="btn--blue remove-btn" data-chatid="${key}">remove</span>
						</div>
					</#list>
					<!-- </div> -->
		    	<!-- </div> -->
		    	<div>debug: ${debugMode}</div><span class="btn--green enable-btn">enable</span><span class="btn--red disable-btn">disable</span>
	    </div>
	    <script>
	    	jQuery(document).ready(function() {
	    		var settingsUrl = location.protocol + "//" + location.hostname + ":" + location.port + "/service/settings";
	    		var choose = "/choose";
	    		var remove = "/remove";
	    		var debug = "/debug";
	    		jQuery('.choose-btn').click(function() {
	    			var chatId = jQuery(this).attr('data-chatid');
	    			jQuery.ajax({
					  	url: settingsUrl + choose + "?chat_id=" + chatId
					}).done(function() {
						console.log("done");
					});
	    		});

	    		jQuery('.remove-btn').click(function() {
	    			var chatId = jQuery(this).attr('data-chatid');
	    			jQuery.ajax({
					  	url: settingsUrl + remove + "?chat_id=" + chatId
					}).done(function() {
						console.log("done");
					});
	    		});
	    		
	    		jQuery('.enable-btn').click(function() {
					jQuery.ajax({
						url: settingsUrl + debug + "?debug=true"
					}).done(function() {
						console.log("changed debug mode");						
					});
				});
					
				jQuery('.disable-btn').click(function() {
					jQuery.ajax({
						url: settingsUrl + debug + "?debug=false"
					}).done(function() {
						console.log("changed debug mode");						
					});
				});
	    	});
	    </script>
    </body>
</html>