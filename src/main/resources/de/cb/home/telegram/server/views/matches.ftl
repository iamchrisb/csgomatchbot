<#-- type="de.cb.home.telegram.bot.views.RssView" -->
<html>
    <header>
        <link rel="stylesheet" href="/css/reset.css" type="text/css">
        <link rel="stylesheet" href="/css/furtive.min.css" type="text/css">
        <link rel="stylesheet" href="/css/styles.css" type="text/css">
        <link rel="stylesheet" href="/css/toggle.css?id=8989" type="text/css">
        <script type="text/javascript" src="/libs/jquery.min.js"></script>
        <script type="text/javascript" src="/libs/countdown.min.js"></script>
    </header>
    <body>
        <div class="container">
            <h3>Upcoming CS:GO matches </h3>
            <ul class="grd accordion">
            <#list matchesOverview.matchDays as matchDay>
                <li class="grd-row">
                    <a class="toggle" href="javascript:void(0);">${matchDay.date}</a>
                    <ul class="inner">
                        <#list matchDay.matches as match>
                            <li>
                                <span>${match.startTime}</span>
                                <span>${match.teamNameA}</span>
                                <span> vs. </span>
                                <span>${match.teamNameB}</span>
                                <a class="btn--blue tick-btn" data-match-id="${match.getMatchId()}">start</a>
                            <#--<a class="btn" target="_blank" alt="view this match on hltv.org" href="${item.link}">view</a>-->
                            </li>
                        </#list>
                    </ul>
                </li>
            <#--<li class="grd-row">-->
            <#--<label class="grd-row-col-4-6">${item.title} (${item.description})</label>-->
            <#--<div class="grd-row-col-2-6">-->
            <#--<span class="timer" data-timestamp="${item.getTimeStamp()}">-->
            <#--00:00:00-->
            <#--</span>-->
            <#--<span>-->
            <#---->
            <#--</span>-->
            <#--<a class="btn--blue tick-btn" data-match-id="${item.getMatchId()}">start</a>-->
            <#--<a class="btn" target="_blank" alt="view this match on hltv.org" href="${item.link}">view</a>-->
            <#--</div>-->
            <#--</li>-->
            </#list>
            </ul>
            <div class="btn--gray stop-btn">stop</div>
        </div>
        <script>
            jQuery(document).ready(function () {

                var format = function (number) {
                    if (number.toString().length < 2) {
                        return "0" + number.toString();
                    } else {
                        return number;
                    }
                }

                /*
                var startTimer = function (end, elem) {
                    setInterval(function () {
                        var start = Date.now();
                        var timeUntilMatch = end - start;
                        var thirtyMin = 1800000;
                        if (timeUntilMatch <= 0) {
                            elem.html('<span class="live">live!</span>');
                        } else {
                            if (timeUntilMatch <= thirtyMin) {
                                elem.addClass('close');
                            }
                            var timor = countdown(start, end, countdown.HOURS | countdown.MINUTES | countdown.SECONDS);
                            var timeString = format(timor.hours) + ":" + format(timor.minutes) + ":" + format(timor.seconds);
                            elem.text(timeString);
                        }
                    }, 1000);
                }
                */

                jQuery('.tick-btn').click(function () {
                    var matchId = jQuery(this).attr("data-match-id");
                    tick(matchId);
                });

                var tick = function (matchId) {
                    var location = window.location;
                    var url = location.protocol + "//" + location.hostname + ":" + location.port + "/service/tick?match_id=" + matchId;
                    jQuery.ajax({
                        url: url,
                        success: function (response) {
                            console.log(response);
                        },
                        error: function (response) {
                            console.log(response);
                        }
                    });
                }

                /*
                var cdSpans = jQuery('.timer');
                cdSpans.each(function (index) {
                    var currentSpan = jQuery(this);
                    var timeStamp = currentSpan.attr('data-timestamp');
                    startTimer(timeStamp, currentSpan);
                });
                */

                jQuery('.toggle').click(function (e) {
                    e.preventDefault();

                    var $this = $(this);

                    if ($this.next().hasClass('show')) {
                        $this.next().removeClass('show');
                        $this.next().slideUp(350);
                    } else {
                        $this.parent().parent().find('li .inner').removeClass('show');
                        $this.parent().parent().find('li .inner').slideUp(350);
                        $this.next().toggleClass('show');
                        $this.next().slideToggle(350);
                    }
                });
            });
        </script>
    </body>
</html>